#!/usr/bin/env python3
# -*- encode: utf-8 -*-

from pathlib import Path
from astropy.time import Time, TimeDelta
from astropy.units import Unit
from tfcat import Feature, Polygon, CRS, FeatureCollection, dump
import numpy
from astroquery.vizier import Vizier

VIZIER_CATALOG_ID = "J/A+AS/119/489"
OBS_FACILITY_STRING = "radiospectrograph of the Astrophysikalisches Institut Potsdam in Tremsdorf"
SCHEMA_URI = "https://voparis-ns.obspm.fr/maser/tfcat/v1.0/tfcat-v1.0.json"


def load_viz_data(viz_id=VIZIER_CATALOG_ID):
    viz_data = Vizier(catalog=viz_id, columns=['**']).query_constraints()[0]
    return viz_data


def resolve_observatory(obsname=OBS_FACILITY_STRING):
    import requests
    service_url = "https://voparis-elasticsearch.obspm.fr/obsfacility/resolve?q="
    result = requests.get(f"{service_url}{obsname}").json()

    facility_name = result['results'][0]['label']
    return facility_name


def event_converter(data):
    for item in data:
        tmin = Time(f"{item['OBSdate']}T{item['OBShs']:02d}:00:00Z") + item['OBSms'] * Unit('min')
        tmax = Time(f"{item['OBSdate']}T{item['OBShe']:02d}:00:00Z") + item['OBSme'] * Unit('min')
        fmin = item['LowFreq'] * Unit("MHz")
        fmax = item['UppFreq'] * Unit("MHz")

        contour = Polygon.from_bbox((tmin.unix, tmax.unix), (fmin.value, fmax.value))

        yield Feature(
            id=item['recno'],
            geometry=contour,
            properties={
                "events": [chunk.strip() for chunk in item['Even'].split(',')],
                "starts_before": item['n_OBSms'] or None,
                "ends_later": item['n_OBSme'] or None,
            }
        )


def converter(viz_id=VIZIER_CATALOG_ID):
    """
    Converts the input data into a TFCat Feature Collection

    :param viz_id: Vizier catalogue ID
    :return: tuple with a dictionary with FeatureCollection values, a year list and a file list
    """

    print(f"Processing catalogue: {VIZIER_CATALOG_ID}")
    # define the global properties
    properties = {
        "instrument_host_name": resolve_observatory(),
        "instrument_name": "radiospectrograph",
        "title": "Catalogue of solar type II radio bursts observed from September 1990 to December 1993",
        "authors": [{
            "FamilyName": "Mann",
            "GivenName": "G.",
        }, {
            "FamilyName": "Klassen",
            "GivenName": "A.",
        }, {
            "FamilyName": "Classen",
            "GivenName": "H.-T.",
        }, {
            "FamilyName": "Aurass",
            "GivenName": "H."
        }, {
            "FamilyName": "Scholz",
            "GivenName": "D."
        }, {
            "FamilyName": "MacDowall",
            "GivenName": "R. J."
        }, {
            "FamilyName": "Stone",
            "GivenName": "R. G."
        }],
        "target_name": "Sun",
        "target_class": "star",
        "target_region": "heliosphere",
        "feature_name": "radio emissions",
        "bib_reference": "1996A&AS..119..489M",
        "data_source_reference": "1996yCat..41190489M",
        "publisher": "Centre de Donnees astronomique de Strasbourg (CDS)",
    }

    # define fields
    fields = {
        'events': {
            'info': 'Kinds of Event',
            'datatype': 'str',
            'ucd': 'meta.code',
            'values': {
                "UNCLF": "unclassified events",
                "B": "single burst",
                "S": "storm bursts",
                "U": "type U bursts",
                "G": "small group of bursts (<10)",
                "GG": "large group of bursts (>10)",
                "C": "any continuum underlying other events",
                "F": "fluctuating continuum",
                "RS": "reverse drift bursts",
                "H": "herringbones",
                "P": "pulsations",
                "Z": "zebra bursts",
                "HARM": "harmonic emission",
                "SPIKES": "spikes bursts",
                "DCIM": "events in the decimetric range"
            }
        },
        'starts_before': {
            'info': 'Note on start time of observation',
            'datatype': 'str',
            'ucd': 'meta.code',
            'values': {
                "E": "early as given",
                "U": "later than given"
            }
        },
        'ends_later': {
            'info': 'Note on end time of observation',
            'datatype': 'str',
            'ucd': 'meta.code',
            'values': {
                "E": "early as given",
                "U": "later than given"
            }
        }
    }

    # define Coordinate Reference System
    crs = CRS.configure(
        time_coords_id="unix",
        spectral_coords_id="MHz",
        ref_position_id=resolve_observatory()
    )

    viz_data = load_viz_data(viz_id)

    collection = FeatureCollection(
        features=list(event_converter(viz_data)),
        properties=properties,
        fields=fields,
        crs=crs
    )

    tfcat_file = Path(__file__).parent / "build" / "vizier_tfcat_mann_1996.json"
    return collection, tfcat_file


def writer(viz_id):
    """writes out a TFCat JSON file

    :param viz_id: Vizier Catalogue ID
    """

    collection, tfcat_file = converter(viz_id)

    print(f"Writing file: {tfcat_file.name}")
    tfcat_file.parent.mkdir(parents=True, exist_ok=True)
    with open(tfcat_file, 'w') as f:
        dump(collection, f)
    print(f"Validating file: {tfcat_file.name}")
    validate(tfcat_file)


def validate(file_name):
    from tfcat.validate import validate_file
    validate_file(file_name, schema_uri="https://voparis-ns.obspm.fr/maser/tfcat/v1.0/tfcat-v1.0.json")


if __name__ == "__main__":
    writer(VIZIER_CATALOG_ID)

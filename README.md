# Mann et al 1996 - Vizier catalogue

This script has been used to produce the TFCat version of Mann et al. (1996) Vizier
catalogue (J/A+AS/119/489).

## Run the example

```bash
python prepare_tfcat.py
```

## References

- Mann G. , Klassen A., Classen H.-T., Aurass H., Scholz D., MacDowall R. J., Stone R. G. 
  (**1996**) *Catalogue of solar type II radio bursts observed from September 1990 to December 
  1993 and their statistical analysis.*. Astron. Astrophys. Suppl., 119, 489-498.
  [bibcode:1996A&AS..119..489M](https://ui.adsabs.harvard.edu/abs/1996A%26AS..119..489M).
- Mann G. , Klassen A., Classen H.-T., Aurass H., Scholz D., MacDowall R. J., Stone R. G.
  (**1996**) *Solar Type II Radio Bursts, 1990.09 - 1993.12 : J/A+AS/119/489*. Vizier (CDS).
  [bibcode:1996yCat..41190489M](https://ui.adsabs.harvard.edu/abs/1996yCat..41190489M)
  

